#from dataclasses import dataclass
#from typing import List
#from ast import literal_eval
from PyQt5.QtCore import pyqtSignal, QObject
import sqlite3
from dataclasses import dataclass
from datetime import datetime
import copy
from typing import List, Dict

DATE_TIME_FORMAT = "%Y-%m-%d %H:%M"
DATE_FORMAT = "%Y-%m-%d"

@dataclass(init=True)
class UserData:
    card_id : str
    start : str
    end : str
    
class SqlDataManager(QObject):
    user_modified = pyqtSignal()

    def __init__(self):
        super().__init__()
        self._connection = sqlite3.connect('./database/data.db')
        self._cursor = self._connection.cursor()
        self._user_today : Dict[int, UserData] = {}
        self.update_user_today()

    def add_user(self, user_id : str, check_in_stamp : str, check_out_stamp : str = None) -> int:
        if check_out_stamp is None:
            select_query = 'INSERT INTO users("card_id", "check_in") ' + \
            f'VALUES("{user_id}", "{check_in_stamp}") '
        else:
            select_query = 'INSERT INTO users("card_id", "check_in", "check_out") ' + \
            f'VALUES("{user_id}", "{check_in_stamp}", "{check_out_stamp}") '
        
        self._cursor.execute(select_query)
        self._cursor.execute("SELECT last_insert_rowid()")
        id_database = self._cursor.fetchone()
        self._connection.commit()
        
        user_data = UserData(user_id, check_in_stamp, check_out_stamp)

        self._user_today[id_database] = user_data
        return id_database

    def update_checkout(self, database_id : int, check_out_stamp : str):
        select_query = f"UPDATE users " + \
        f"SET check_out = '{check_out_stamp}' " + \
        f"WHERE id = {database_id}; "

        self._user_today[database_id].end = check_out_stamp
        self._cursor.execute(select_query)
        self._connection.commit()

    def get_without_checkout(self):
        select_query = f"SELECT id, card_id, check_in, check_out " + \
        f"FROM users " + \
        "WHERE check_out IS NULL;"

        self._cursor.execute(select_query)
        records = self._cursor.fetchall()
        return records

    def update_user_today(self):
        date_time_obj = datetime.now()
        date_time_str = date_time_obj.strftime(DATE_FORMAT)
        select_query = 'SELECT id, card_id, check_in, check_out ' + \
        'FROM users ' + \
        f"WHERE DATE(check_in)=DATE('{date_time_str}');"
        self._cursor.execute(select_query)
        records = self._cursor.fetchall()

        for row in records:
            user_data = UserData(row[1], row[2], row[3])
            self._user_today[row[0]] = user_data

    def get_users_between_time(self, date_time_start : str, date_time_end : str) -> Dict[int, UserData]:
        select_query = 'SELECT id, card_id, check_in, check_out ' + \
        'FROM users ' + \
        f"WHERE check_in <= '{date_time_end}' " + \
        f"AND check_out >= '{date_time_start}';"
        self._cursor.execute(select_query)
        records = self._cursor.fetchall()
        users = {}
        for row in records:
            user_data = UserData(row[1], row[2], row[3])
            users[row[0]] = user_data

        return users

    def get_user(self, blue_card_id : str, fitness_card_id : str) -> Dict[int, UserData]:
        select_query = 'SELECT id, card_id, check_in, check_out ' + \
        'FROM users ' + \
        f"WHERE card_id = '{blue_card_id}' or card_id = '{fitness_card_id}';"
        self._cursor.execute(select_query)
        records = self._cursor.fetchall()
        users = {}
        for row in records:
            user_data = UserData(row[1], row[2], row[3])
            users[row[0]] = user_data

        return users

    def users_today(self) -> Dict[int, UserData]:
        return copy.copy(self._user_today) 