#!/bin/bash
script=`realpath $0`
parentdir="$(dirname "$script")"
layout_qt_path="$(dirname "$parentdir")"/layouts
layout_python_path="$(dirname "$parentdir")"/src/python_layouts

array=("layout")

for i in "${array[@]}"; do   # The quotes are necessary here
    ui_file=${layout_qt_path}/${i}.ui
    py_file=${layout_python_path}/${i}.py
    pyuic5 -x $ui_file -o $py_file
done