#!/usr/bin/python3
import sys
import time
import sqlite3
from typing import Set
import xlsxwriter
from datetime import datetime
from dataclasses import dataclass
from PyQt5.QtWidgets import QApplication, QMainWindow, QListWidgetItem, QDialog, QHeaderView, QTableWidgetItem
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread, QObject, QSize
from src import Layout, UserData, SqlDataManager, DATE_TIME_FORMAT
from fpdf import FPDF

class App(Layout, QObject):
    def __init__(self):   
        super().__init__()
        self.app = QApplication(sys.argv)
        self.main_window = QMainWindow()
        self.gui = Layout()
        self.gui.setupUi(self.main_window)
        self.setupUi(self.main_window)
        self._data = SqlDataManager()
        self._checked_in_users = {}
        self._checked_in_ids = {}
        self._last_start_time = ""
        self._last_end_time = ""
        self._search_user_set = set({})

        
        #self.tabWidget.setBackgroundRole()
        self.main_window.resize(1920, 1080)
        self.start_time_edit.setMinimumSize(QSize(250, 0))
        self.end_time_edit.setMinimumSize(QSize(250, 0))

        self.tabWidget.setStyleSheet("QTabWidget::pane { /* The tab widget frame */border-top: 2px solid #C2C7CB;}")
        self.tabWidget.setAutoFillBackground(False)

        self.today_list.horizontalHeader().setVisible(True)
        self.today_list.verticalHeader().setVisible(True)
        self.today_list.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

        self.time_search_list.horizontalHeader().setVisible(True)
        self.time_search_list.verticalHeader().setVisible(True)
        self.time_search_list.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

        self.user_search_list.horizontalHeader().setVisible(True)
        self.user_search_list.verticalHeader().setVisible(True)
        self.user_search_list.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

        # connect slots
        self.check_in_button.clicked.connect(self.check_in_button_clicked)
        self.checkout_button.clicked.connect(self.check_out_button_clicked)
        self.search_button.clicked.connect(self.search_users_button_clicked)
        self.add_search_button.clicked.connect(self.add_search_button_clicked)
        self.search_id_button.clicked.connect(self.search_dates_button_clicked)
        self.export_users_button.clicked.connect(self.export_users)
        self.export_dates_button.clicked.connect(self.export_dates)
        

        self.update_without_checkout()
        self.update_today_list()

    def show(self):
        self.main_window.show()
        sys.exit(self.app.exec_())

    @pyqtSlot()
    def check_in_button_clicked(self):
        id_string = self.card_id_edit.text()
        if id_string == "":
            return
        
        date_time_obj = datetime.now()
        date_time_str = date_time_obj.strftime(DATE_TIME_FORMAT)
        
        if id_string in self._checked_in_users:
            self.checkout_user(id_string, date_time_str)
        else:
            database_id = self._data.add_user(id_string, date_time_str)
            self._checked_in_users[id_string] = date_time_str
            self._checked_in_ids[id_string] = database_id

        self.update_user_list()
        self.card_id_edit.clear()

    @pyqtSlot()
    def search_users_button_clicked(self, reset = True):
        self.add_users_to_table()

    @pyqtSlot()
    def add_search_button_clicked(self):
        self.add_users_to_table(False)

    @pyqtSlot()
    def search_dates_button_clicked(self):
        bluecard_id = self.bluecard_search_edit.text()
        fitnesscard_id = self.firnesscard_search_edit.text()
        entries = self._data.get_user(bluecard_id, fitnesscard_id)
        self.user_search_list.setRowCount(0)
        for database_id, user in entries.items():
            self.add_table_list(self.user_search_list, database_id, user)

    def add_users_to_table(self, reset=True):
        check_in_time = self.start_time_edit.dateTime().toString(Qt.ISODate)
        check_out_time = self.end_time_edit.dateTime().toString(Qt.ISODate)
        check_in_time = check_in_time[:10] + " " + check_in_time[11:]
        check_out_time = check_out_time[:10] + " " + check_out_time[11:]
        self._last_start_time = check_in_time
        self._last_end_time = check_out_time
        users = self._data.get_users_between_time(check_in_time, check_out_time)
        if reset:
            self.time_search_list.setRowCount(0)
            self._search_user_set = set({})
        for database_id, user in users.items():
            if database_id not in self._search_user_set:
                self.add_table_list(self.time_search_list, database_id, user)
                self._search_user_set.add(database_id)

    def checkout_user(self, card_id : str, checkout_date : str):
        check_in_date_str = self._checked_in_users.pop(card_id, None)
        database_id = self._checked_in_ids.pop(card_id, None)
        self._data.update_checkout(database_id, checkout_date)
        self.add_to_today_list(database_id, card_id, check_in_date_str, checkout_date)
        
   
    @pyqtSlot()
    def check_out_button_clicked(self):
        selected_items = self.check_in_list.selectedItems()
        if not selected_items:
            return
        item = selected_items[0]
        id_string = item.data(Qt.UserRole)
        
        date_time_obj = datetime.now()
        date_time_str = date_time_obj.strftime(DATE_TIME_FORMAT)

        if id_string in self._checked_in_users:
            self.checkout_user(id_string, date_time_str)

        self.update_user_list()

    def update_user_list(self):
        self.check_in_list.clear()
        for key, check_in in self._checked_in_users.items():
            item_str = f"Card-ID: {key}, Check-In: {check_in}"
            item = QListWidgetItem()
            item.setText(item_str)
            item.setData(Qt.UserRole, key)
            self.check_in_list.addItem(item)

    def add_to_today_list(self, database_id : int, card_id : str, start : str, end : str):
        user = UserData(card_id, start, end)
        self.add_table_list(self.today_list,database_id, user)

    def add_to_time_search_list(self, database_id : int, card_id : str, start : str, end : str):
        user = UserData(card_id, start, end)
        self.add_table_list(self.time_search_list, database_id, user)

    def add_table_list(self, widget, database_id : int, user : UserData):
        row_count = widget.rowCount()
        widget.insertRow(row_count)
        user_id_item = QTableWidgetItem()
        user_id_item.setText(user.card_id)
        user_id_item.setData(Qt.UserRole, database_id)
        widget.setItem(row_count, 0, user_id_item)
        check_in_item = QTableWidgetItem()
        check_in_item.setText(user.start)
        widget.setItem(row_count, 1, check_in_item)
        check_out_item = QTableWidgetItem()
        check_out_item.setText(user.end)
        widget.setItem(row_count, 2, check_out_item)


    def update_today_list(self):
        users_today = self._data.users_today()
        for key, user in users_today.items():
            if user.end is not None:
                self.add_to_today_list(key, user.card_id, user.start, user.end)

    def export_dates(self):
        print("export dates")

    def export_users(self):
        self.create_pdf_of_users()
        self.create_excel_of_users()

    def create_pdf_of_users(self):
        pdf = FPDF()   
        pdf.add_page()
        pdf.set_font("Arial", size = 14)
        head_str = f"Trainierende zwischen {self._last_start_time}Uhr und {self._last_end_time}Uhr"
        pdf.cell(0, 10, txt = head_str, ln = 1, align = 'C')
        pdf.set_font("Arial", size = 11)
        for row_index in range(self.time_search_list.rowCount()):
            card_id = self.time_search_list.item(row_index, 0).text()
            check_in = self.time_search_list.item(row_index, 1).text()
            check_out = self.time_search_list.item(row_index, 2).text()
            row_str = f"Karten ID: {card_id}, Check-In: {check_in}, Check-Out: {check_out}"
            print(row_str)
            pdf.cell(0, 6, txt = row_str, ln = 1, align = 'L')
        pdf.output("users.pdf")

    def create_excel_of_users(self):   
        workbook = xlsxwriter.Workbook('users.xlsx')
        worksheet = workbook.add_worksheet()
        bold = workbook.add_format({'bold': 1})
        date_format = workbook.add_format({'num_format': 'dd/mm/yyyy hh:mm'})
        worksheet.write('A1', 'Name', bold)
        worksheet.write('B1', 'Adresse', bold)
        worksheet.write('C1', 'E-Mail', bold)
        worksheet.write('D1', 'Telefon', bold)
        worksheet.write('E1', 'Karten-ID', bold)
        worksheet.write('F1', 'Check-In', bold)
        worksheet.write('G1', 'Check-Out', bold)

        row = 1
        col = 4

        for row_index in range(self.time_search_list.rowCount()):
            card_id = self.time_search_list.item(row_index, 0).text()
            check_in = self.time_search_list.item(row_index, 1).text()
            check_in = datetime.strptime(check_in, '%Y-%m-%d %H:%M')
            check_out = self.time_search_list.item(row_index, 2).text()
            check_out = datetime.strptime(check_out, '%Y-%m-%d %H:%M')

            #sprint(check_out)
            worksheet.write_string(row, col, card_id)
            worksheet.write_datetime(row, col + 1, check_in, date_format)
            worksheet.write_datetime(row, col + 2, check_out, date_format)
            row += 1

        workbook.close()

    def update_without_checkout(self): 
        records = self._data.get_without_checkout()
        for record in records:
            self._checked_in_users[record[1]] = record[2]
            self._checked_in_ids[record[1]] = record[0]
        self.update_user_list()

if __name__=="__main__":
    a = App()
    a.show()
